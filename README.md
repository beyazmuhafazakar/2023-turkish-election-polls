# 2023 Turkish Election Polls
Dependencies:


- [ggplot2](https://ggplot2.tidyverse.org/)
- [tidyr](https://tidyr.tidyverse.org/)
- [anytime](https://dirk.eddelbuettel.com/code/anytime.html)

`install.packages(c("ggplot2", "tidyr", "anytime"))`

The dates are the publication date for a given poll.
